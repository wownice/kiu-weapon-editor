public class Weapon
{
	private int slotNumber;
	private String weaponName;
	private String[] weaponBytesDec; // length of 3
	private double totalValue;
	private Attribute[] weaponAttributes; // length of 6
	private Stars meleeStars;
	private Stars rangedStars;

	//attributes

	public Weapon(int slot, String name, String[] dec, Attribute[] attr, Stars melee, Stars ranged)
	{
		slotNumber = slot;
		weaponName = name;
		weaponBytesDec = dec;
		weaponAttributes = attr;
		meleeStars = melee;
		rangedStars = ranged;

		updateTotalValue();
	}

	public Weapon(){}

	public void setSlotNumber(int newNum)
	{
		slotNumber = newNum;
	}

	public void setWeaponName(String newName)
	{
		weaponName = newName;
	}

	public void setMeleeStars(Stars newStars)
	{
		meleeStars = newStars;
	}

	public void setRangedStars(Stars newStars)
	{
		rangedStars = newStars;
	}

	public void setWeaponBytesDec(String[] newBytes)
	{
		weaponBytesDec = newBytes;
	}

	public void setWeaponAttributes(Attribute[] newAttributes)
	{
		weaponAttributes = newAttributes;
	}

	public void changeWeapon(String newName, String[] newDecBytes)
	{
		setWeaponName(newName);
		setWeaponBytesDec(newDecBytes);
	}

	public int getSlotNumber()
	{
		return slotNumber;
	}

	public String getWeaponName()
	{
		return weaponName;
	}

	public Stars getMeleeStars()
	{
		return meleeStars;
	}

	public String getMeleeStarsAmount()
	{
		return meleeStars.getAmount();
	}

	public double getMeleeStarsValue()
	{
		return meleeStars.getValue();
	}

	public Stars getRangedStars()
	{
		return rangedStars;
	}

	public String getRangedStarsAmount()
	{
		return rangedStars.getAmount();
	}

	public double getRangedStarsValue()
	{
		return rangedStars.getValue();
	}

	public String[] getWeaponBytesDec()
	{
		return weaponBytesDec;
	}

	public Attribute[] getWeaponAttributes()
	{
		return weaponAttributes;
	}

	public void updateTotalValue()
	{
		totalValue = 100 + getMeleeStarsValue() + getRangedStarsValue()
				+ weaponAttributes[0].getValue() + weaponAttributes[1].getValue() +
				weaponAttributes[2].getValue() + weaponAttributes[3].getValue() +
				weaponAttributes[4].getValue() + weaponAttributes[5].getValue();
	}

	public String getTotalValue()
	{
		return String.format("%.2f", totalValue) + "";
	}

}