import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
public class Database
{
	private ArrayList<Weapon> weaponList;
	private ArrayList<Attribute> attributeList;
	private ArrayList<Stars> meleeStarsList;
	private ArrayList<Stars> rangedStarsList;

	private final InputStream weaponIS = getClass().getResourceAsStream("weapons.csv");
	private final BufferedReader weaponBR = new BufferedReader(new InputStreamReader(weaponIS));
	private final InputStream attributesIS = getClass().getResourceAsStream("attributes.csv");
	private final BufferedReader attributesBR = new BufferedReader(new InputStreamReader(attributesIS));
	private final InputStream starsMeleeIS = getClass().getResourceAsStream("stars_melee.csv");
	private final BufferedReader starsMeleeBR = new BufferedReader(new InputStreamReader(starsMeleeIS));
	private final InputStream starsRangedIS = getClass().getResourceAsStream("stars_ranged.csv");
	private final BufferedReader starsRangedBR = new BufferedReader(new InputStreamReader(starsRangedIS));


	private Scanner scans;

	public Database()
	{
		weaponList = new ArrayList<Weapon>();
		attributeList = new ArrayList<Attribute>();
		meleeStarsList = new ArrayList<Stars>();
		rangedStarsList = new ArrayList<Stars>();

		fillWeaponList();
		fillAttributeList();
		fillStarsList(meleeStarsList, starsMeleeBR);
		fillStarsList(rangedStarsList, starsRangedBR);

	}

	public void fillWeaponList()
	{//name,decbyte1,decbyte2,decbyte3
		scans = new Scanner(weaponBR);
		String currentLine;
		String[] rowData;
		String[] weaponBytes;
		while (scans.hasNextLine())
		{
			currentLine = scans.nextLine();
			rowData = currentLine.split(",");

			Weapon newWeap = new Weapon();
			newWeap.setWeaponName(rowData[0]);

			//dec
			weaponBytes = new String[3];
			weaponBytes[0] = rowData[1];
			weaponBytes[1] = rowData[2];
			weaponBytes[2] = rowData[3];
			newWeap.setWeaponBytesDec(weaponBytes);

			weaponList.add(newWeap);
		}
		scans.close();
	}

	public void fillAttributeList()
	{	//name,dec,value
		scans = new Scanner(attributesBR);
		String currentLine;
		String[] rowData;
		String attributeName;
		String byteValueDec;
		String value;
		while (scans.hasNextLine())
		{
			currentLine = scans.nextLine();
			rowData = currentLine.split(",");

			attributeName = rowData[0];
			byteValueDec = rowData[1];//dec
			value = rowData[2];
			Attribute newAttribute = new Attribute(attributeName, byteValueDec, value);
			attributeList.add(newAttribute);
		}
		scans.close();
	}

	public void fillStarsList(ArrayList<Stars> starList, BufferedReader file)
	{//name,dec,value
		scans = new Scanner(file);
		String currentLine;
		String[] rowData;
		String starsAmount;
		String byteValueDec;
		String value;
		while (scans.hasNextLine())
		{
			currentLine = scans.nextLine();
			rowData = currentLine.split(",");

			starsAmount = rowData[0];
			byteValueDec = rowData[1];//dec
			value = rowData[2];

			Stars newStar = new Stars(starsAmount, byteValueDec, value);
			starList.add(newStar);
		}
		scans.close();
	}

	public ArrayList<Weapon> getWeaponList()
	{
		return weaponList;
	}

	public ArrayList<Attribute> getAttributeList()
	{
		return attributeList;
	}

	public ArrayList<Stars> getRangedStarsList()
	{
		return rangedStarsList;
	}

	public ArrayList<Stars> getMeleeStarsList()
	{
		return meleeStarsList;
	}

	public Stars getMeleeStarsFromDBWithDecimal(String decBytes)
	{
		for(Stars s : meleeStarsList)
		{
			if(s.getDec().equals(decBytes))
			{
				return s;
			}
		}

		return meleeStarsList.get(0);// 0 stars
	}

	public Stars getRangedStarsFromDBWithDecimal(String decBytes)
	{
		for(Stars s : rangedStarsList)
		{
			if(s.getDec().equals(decBytes))
			{
				return s;
			}
		}

		return rangedStarsList.get(0);// 0 stars
	}

	public Attribute getAttributeOffDbWithDec(String val)//takes dec value to get attribute name
	{
		for(Attribute a : attributeList)
		{
			if(a.getValueDec().equals(val))
			{
				return a;
			}
		}

		return attributeList.get(0);//returns Blank(none) attribute
	}

	public String getWeaponNameOffDbWithDec(String[] bytes)
	{
		for(Weapon w : weaponList)
		{
			String[] bytesList = w.getWeaponBytesDec();
			if(bytesList[0].equals(bytes[0]) && bytesList[1].equals(bytes[1])
					&& bytesList[2].equals(bytes[2]))
			{
				return w.getWeaponName();
			}
		}

		return "Unknown";//returns blank
	}

	public int getWeaponIndex(String name)
	{
		for(int i = 0; i < weaponList.size(); i++)
		{
			if(name.equals(weaponList.get(i).getWeaponName()))
			{
				return i;
			}
		}
		return 0;
	}

	public int getAttributeIndex(String attribute)
	{
		for(int i = 0; i < attributeList.size(); i++)
		{
			if(attribute.equals(attributeList.get(i).getName()))
			{
				return i;
			}
		}
		return 0;
	}

	public int getRangedStarsIndex(String amount)
	{
		for(int i = 0; i < rangedStarsList.size(); i++)
		{
			if(amount.equals(rangedStarsList.get(i).getAmount()))
			{
				return i;
			}
		}
		return 0;
	}

	public int getMeleeStarsIndex(String amount)
	{
		for(int i = 0; i < meleeStarsList.size(); i++)
		{
			if(amount.equals(meleeStarsList.get(i).getAmount()))
			{
				return i;
			}
		}
		return 0;
	}

	public int searchWeapon(String searchitem, int curIndex)
	{
		for(int i = curIndex; i < weaponList.size(); i++)
		{
			if(weaponList.get(i).getWeaponName().toLowerCase().startsWith(searchitem.toLowerCase()))
			{
				return i + 1;
			}
		}

		//in case no results come up after current index
		for(int i = 0; i < weaponList.size(); i++)
		{
			if(weaponList.get(i).getWeaponName().toLowerCase().startsWith(searchitem.toLowerCase()))
			{
				return i + 1;
			}
		}
		//if all else fails
		return -1;
	}

	public int searchAttribute(String searchitem, int curIndex)
	{
		for(int i = curIndex + 1; i < attributeList.size(); i++)
		{
			if(attributeList.get(i).getName().toLowerCase().startsWith(searchitem.toLowerCase()))
			{
				return i;
			}
		}

		//in case no results come up after current index
		for(int i = 0; i < attributeList.size(); i++)
		{
			if(attributeList.get(i).getName().toLowerCase().startsWith(searchitem.toLowerCase()))
			{
				return i;
			}
		}
		//if all else fails
		return -1;
	}
}
