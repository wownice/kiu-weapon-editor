import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class EditTools
{
	private File gameSave;
	private RandomAccessFile raf;
	private ArrayList<Weapon> weaponList; //the weapon list that came with the save
	private ArrayList<Weapon> newWeaponList; //the weapon list that is changed

	private final Database db = new Database();

	public EditTools(File file) throws IOException
	{
		gameSave = file;
		raf = new RandomAccessFile(gameSave, "rw");
		readFile();
	}

	public void readFile() throws IOException //reads into decimal
	{
		weaponList = new ArrayList<Weapon>();
		//seek takes decimal then converts to hex
		int curSeekSpot = 1653;//weapons starting point; weapons separated by 20 in HEX
		//32 in decimal
		int slotNumber = 1; //cur weapon slot

		while(curSeekSpot <= 33648)
		{
			Weapon weapon = new Weapon();

			raf.seek(curSeekSpot);
			String[] weaponByteList = new String[3];

			int zeroCount = 0;
			boolean addableToList;
			for(int j = 0; j < 3; j++)//determining the weapon -- 3 bytes long
			{
				String weaponByteRead = raf.read() + "";
				addableToList = true;
				if(weaponByteRead.equals("0"))
				{
					zeroCount++;
				}
				if(zeroCount >= 3)
				{
					curSeekSpot = Integer.MAX_VALUE;
					addableToList = false;
				}
				if(addableToList)
				{
					weaponByteList[j] = weaponByteRead;
				}
			}

			weapon.setWeaponName(db.getWeaponNameOffDbWithDec(weaponByteList));

			if(curSeekSpot != Integer.MAX_VALUE)
			{
				weapon.setWeaponBytesDec(weaponByteList);
				weapon.setSlotNumber(slotNumber);

				Stars star;
				String starValue;
				starValue = raf.read() + "";
				star = db.getRangedStarsFromDBWithDecimal(starValue);
				weapon.setRangedStars(star);//ranged stars are right after weapon bytes

				raf.seek(curSeekSpot + 5);
				starValue = raf.read() + "";
				star = db.getMeleeStarsFromDBWithDecimal(starValue);
				weapon.setMeleeStars(star);
				//melee stars 5 away from beginning of weapon bytes

				Attribute[] weaponAttributesList = new Attribute[6];//6 attributes
				Attribute attribute;
				int curAttribute = 0;
				for(int i = 15; i <= 25; i+=2) //15 is first attribute, 25 is last
				{
					raf.seek(curSeekSpot + i);
					String attributeVal = raf.read() + "";
					attribute = db.getAttributeOffDbWithDec(attributeVal);
					weaponAttributesList[curAttribute] = attribute;
					curAttribute++;
				}

				weapon.setWeaponAttributes(weaponAttributesList);
				weaponList.add(weapon);

				slotNumber++;
				curSeekSpot += 32;
			}
		}

		createNewWeaponList();
	}

	public ArrayList<Weapon> getWeaponList()
	{
		return weaponList;
	}

	public ArrayList<Weapon> getNewWeaponList()
	{
		return newWeaponList;
	}

	public Database getDatabase()
	{
		return db;
	}

	public void createNewWeaponList()
	{
		newWeaponList = new ArrayList<Weapon>();
		for(Weapon w : weaponList)
		{
			Weapon newWeapon = new Weapon(w.getSlotNumber(), w.getWeaponName(),
					w.getWeaponBytesDec(), w.getWeaponAttributes(), w.getMeleeStars(),
					w.getRangedStars());
			newWeaponList.add(newWeapon);
		}
	}

	// modify the new weapon list
	public void updateListWeapon(int oldWeaponIndex, String newWeaponName)
	{
		//oldWeaponIndex is correct when taken from currentSlotNum
		int newWeaponIndex = db.getWeaponIndex(newWeaponName);
		Weapon weap;
		if(newWeaponIndex == db.getWeaponList().size()) // for UNCHANGED weapons
		{
			weap = weaponList.get(oldWeaponIndex);
		}
		else
		{
			weap = db.getWeaponList().get(newWeaponIndex);
		}
		newWeaponList.get(oldWeaponIndex).changeWeapon(weap.getWeaponName(), weap.getWeaponBytesDec());
	}

	public void updateListAttribute(int weaponIndex, int oldAttributeIndex, String newAttributeName)
	{
		int newAttributeIndex = db.getAttributeIndex(newAttributeName);
		Attribute newAttribute = db.getAttributeList().get(newAttributeIndex);
		newWeaponList.get(weaponIndex).getWeaponAttributes()[oldAttributeIndex] =
				new Attribute(newAttribute.getName(), newAttribute.getValueDec(), newAttribute.getValue() + "");
		newWeaponList.get(weaponIndex).updateTotalValue();
	}

	public void updateListMeleeStars(int weaponIndex, String newValue)
	{
		int newStarIndex = db.getMeleeStarsIndex(newValue);
		Stars star = db.getMeleeStarsList().get(newStarIndex);
		newWeaponList.get(weaponIndex).setMeleeStars(new Stars(star.getAmount(),
				star.getDec(), star.getValue() + ""));
		newWeaponList.get(weaponIndex).updateTotalValue();
	}

	public void updateListRangedStars(int weaponIndex, String newValue)
	{
		int newStarIndex = db.getRangedStarsIndex(newValue);
		Stars star = db.getRangedStarsList().get(newStarIndex);
		newWeaponList.get(weaponIndex).setRangedStars(new Stars(star.getAmount(),
				star.getDec(), star.getValue() + ""));
		newWeaponList.get(weaponIndex).updateTotalValue();
	}

	public void saveFile() throws NumberFormatException, IOException
	{
		int curSeekSpot = 1653;

		for(int i = 0; i < newWeaponList.size();i++)
		{
			Weapon curWeap = newWeaponList.get(i);

			raf.seek(curSeekSpot);
			for(int j = 0; j < 3; j++)
			{
				raf.write(Integer.parseInt(curWeap.getWeaponBytesDec()[j]));
			}

			raf.write(Integer.parseInt(curWeap.getRangedStars().getDec()));

			raf.seek(curSeekSpot + 5);
			raf.write(Integer.parseInt(curWeap.getMeleeStars().getDec()));

			int curAttribute = 0;
			for(int k = 15; k <= 25; k+=2) //15 is first attribute, 25 is last
			{
				raf.seek(curSeekSpot + k);
				raf.write(Integer.parseInt(curWeap.getWeaponAttributes()[curAttribute].getValueDec()));
				curAttribute++;
			}
			curSeekSpot += 32;
		}
	}
}
