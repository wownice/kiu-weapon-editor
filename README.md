Edits existing weapons for Kid Icarus Uprising.
This is editing only, not adding. Weapons and their attributes will be able to be changed.
This program has trouble determing weapons if they do not have base stats. 
It can be alleviated by comparing loaded weapons with weapon attributes in game.
This editor will display point values for stars and attributes as well as calculate total. 
If stars and attribute point values total over a limit, they will no longer have any effect. 
If the total is illegal, it will be displayed in red. This does not stop you from saving it to file.