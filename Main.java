import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class Main extends Application
{
	private Stage theStage;
	private Scene theScene;
	private BorderPane borderPane;

	private File saveFile;

	private EditTools editTools;
	private Database db;

	private final StackPane tableHolder = new StackPane();
	private final TableView table = new TableView();
	private final TableColumn slotCol = new TableColumn("Slot");
	private final HBox uiBox = new HBox(20);
	private final VBox weaponDetailsBox = new VBox(10);

	private int currentSlotNum;
	private boolean isSlotChange;

	private final ComboBox weaponComboBox = new ComboBox();

	private final ComboBox meleeStarsComboBox = new ComboBox();//choices .5-6
	private final ComboBox rangedStarsComboBox = new ComboBox();//choices .5-6

	private final ComboBox weaponSlot1ComboBox = new ComboBox();
	private final ComboBox weaponSlot2ComboBox = new ComboBox();
	private final ComboBox weaponSlot3ComboBox = new ComboBox();
	private final ComboBox weaponSlot4ComboBox = new ComboBox();
	private final ComboBox weaponSlot5ComboBox = new ComboBox();
	private final ComboBox weaponSlot6ComboBox = new ComboBox();

	private final Label curWeaponValueLabel = new Label();
	private final Label meleeStarsValueLabel = new Label();
	private final Label rangedStarsValueLabel = new Label();
	private final Label weaponSlot1ValueLabel = new Label();
	private final Label weaponSlot2ValueLabel = new Label();
	private final Label weaponSlot3ValueLabel = new Label();
	private final Label weaponSlot4ValueLabel = new Label();
	private final Label weaponSlot5ValueLabel = new Label();
	private final Label weaponSlot6ValueLabel = new Label();

	public static void main(String[] args)
	{
		Application.launch(args);
	}

	@Override
	public void start(Stage stage)
	{
		theStage = stage;
		showUi();
	}

	public void showUi()
	{
		//file management bar
		FileChooser fileChooser = new FileChooser();
		Menu fileMenu = new Menu("File");
		MenuItem loadSave = new MenuItem("Load file");
		MenuItem saveSave = new MenuItem("Save file");
		MenuBar menuBar = new MenuBar();

		Button saveOKButton = new Button("OK");
		VBox saveVBox = new VBox(5);
		Scene tempScene = new Scene(saveVBox);
		Stage tempStage = new Stage();
		tempStage.setResizable(false);
		saveVBox.setAlignment(Pos.CENTER);
		saveVBox.getChildren().addAll(new Label("Save Complete."), saveOKButton);
		saveOKButton.setOnAction(new EventHandler<ActionEvent>()
		{
            @Override
            public void handle(ActionEvent event)
            {
                tempStage.close();
            }
        });

		fileChooser.setTitle("Open SAV File");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("SAV File", "01.SAV","02.SAV","03.SAV"));
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

		loadSave.setOnAction(new EventHandler<ActionEvent>()
		{
		    @Override
		    public void handle(ActionEvent e)
		    {
		    	saveFile = fileChooser.showOpenDialog(theStage);
				if (saveFile != null)
				{
					try {
						fillUiWithSaveData(saveFile);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
		    }
		});

		saveSave.setOnAction(new EventHandler<ActionEvent>()
		{
		    @Override
		    public void handle(ActionEvent e)
		    {
		    	if (saveFile != null)
				{
		    		//saving file
		    		try {
						editTools.saveFile();
					} catch (NumberFormatException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

		    		tempStage.setScene(tempScene);
		    		tempStage.show();
				}
		    }
		});
		fileMenu.getItems().addAll(loadSave,saveSave);
		menuBar.getMenus().add(fileMenu);

		borderPane = new BorderPane();
		borderPane.setTop(menuBar);
		borderPane.setCenter(uiBox);

		weaponComboBox.setMinWidth(200);
		weaponSlot1ComboBox.setMinWidth(200);
		weaponSlot2ComboBox.setMinWidth(200);
		weaponSlot3ComboBox.setMinWidth(200);
		weaponSlot4ComboBox.setMinWidth(200);
		weaponSlot5ComboBox.setMinWidth(200);
		weaponSlot6ComboBox.setMinWidth(200);

		table.getColumns().add(slotCol);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		slotCol.setStyle( "-fx-alignment: CENTER;");
		tableHolder.getChildren().add(table);
		tableHolder.setMaxWidth(80);

		//Current Weapon
		HBox weaponBox = new HBox(20);
		weaponBox.setPadding(new Insets(10,0,10,0));
		weaponBox.getChildren().addAll(new Label("Weapon:"),
				weaponComboBox, new Label("Value:"), curWeaponValueLabel);
		//Current Weapon

		// Stars
		HBox starsBox = new HBox(100);
		starsBox.setAlignment(Pos.CENTER);
		VBox meleeStarsVBox = new VBox(10);
		meleeStarsVBox.getChildren().addAll(new Label("Melee Stars"), meleeStarsComboBox
				, new Label("Value:"), meleeStarsValueLabel);
		VBox rangedStarsVBox = new VBox(10);
		rangedStarsVBox.getChildren().addAll(new Label("Ranged Stars"), rangedStarsComboBox
				, new Label("Value:"), rangedStarsValueLabel);
		starsBox.getChildren().addAll(meleeStarsVBox, rangedStarsVBox);
		// Stars

		//Attributes
		HBox attribute1HBox = new HBox(20);
		attribute1HBox.getChildren().addAll(weaponSlot1ComboBox, new Label("Value:"), weaponSlot1ValueLabel);
		HBox attribute2HBox = new HBox(20);
		attribute2HBox.getChildren().addAll(weaponSlot2ComboBox, new Label("Value:"), weaponSlot2ValueLabel);
		HBox attribute3HBox = new HBox(20);
		attribute3HBox.getChildren().addAll(weaponSlot3ComboBox, new Label("Value:"), weaponSlot3ValueLabel);
		HBox attribute4HBox = new HBox(20);
		attribute4HBox.getChildren().addAll(weaponSlot4ComboBox, new Label("Value:"), weaponSlot4ValueLabel);
		HBox attribute5HBox = new HBox(20);
		attribute5HBox.getChildren().addAll(weaponSlot5ComboBox, new Label("Value:"), weaponSlot5ValueLabel);
		HBox attribute6HBox = new HBox(20);
		attribute6HBox.getChildren().addAll(weaponSlot6ComboBox, new Label("Value:"), weaponSlot6ValueLabel);

		VBox attributesBox = new VBox(10);
		attributesBox.getChildren().addAll(new Label("Attributes"), attribute1HBox, attribute2HBox,
				attribute3HBox, attribute4HBox, attribute5HBox, attribute6HBox);
		//Attributes

		weaponDetailsBox.getChildren().addAll(weaponBox, starsBox, attributesBox);
		uiBox.getChildren().addAll(tableHolder, weaponDetailsBox);

		theScene = new Scene(borderPane);
		theStage.setMinWidth(500);
		theStage.setMinHeight(500);
		theStage.setResizable(false);
		theStage.setTitle("Kid Icarus: Uprising Weapon Editor");
		theStage.setScene(theScene);
		theStage.show();
	}

	public void fillUiWithSaveData(File saveFile) throws IOException
	{
		clearAll();
		editTools = new EditTools(saveFile);
		db = editTools.getDatabase();

		ObservableList<Weapon> slotObservableList = FXCollections.observableArrayList(editTools.getNewWeaponList());
		slotCol.setCellValueFactory(
			    new PropertyValueFactory<Weapon,String>("slotNumber")
			);
		table.setItems(slotObservableList);

		fillWeaponComboBox();
		fillMeleeStarsBox();
		fillRangedStarsBox();
		fillAttributeBoxes();

		setListeners();
		table.getSelectionModel().selectFirst();
	}

	public void fillWeaponComboBox()
	{
		int dbListLength = db.getWeaponList().size();

		for(int i = 0; i < dbListLength; i++)
		{
			weaponComboBox.getItems().add(db.getWeaponList().get(i).getWeaponName());
		}

		weaponComboBox.getItems().add(0, "");
		// here for table's change listener on startup
	}

	public void fillMeleeStarsBox()
	{
		for(int i = 0; i < db.getMeleeStarsList().size(); i++)
		{
			meleeStarsComboBox.getItems().add(db.getMeleeStarsList().get(i).getAmount());
		}
	}

	public void fillRangedStarsBox()
	{
		for(int i = 0; i < db.getRangedStarsList().size(); i++)
		{
			rangedStarsComboBox.getItems().add(db.getRangedStarsList().get(i).getAmount());
		}
	}

	public void fillAttributeBoxes()
	{
		fillAttributeBox(weaponSlot1ComboBox);
		fillAttributeBox(weaponSlot2ComboBox);
		fillAttributeBox(weaponSlot3ComboBox);
		fillAttributeBox(weaponSlot4ComboBox);
		fillAttributeBox(weaponSlot5ComboBox);
		fillAttributeBox(weaponSlot6ComboBox);
	}

	public void fillAttributeBox(ComboBox cb)
	{
		for(int i = 0; i < db.getAttributeList().size(); i++)
		{
			cb.getItems().add(db.getAttributeList().get(i).getName());
		}
	}

	public void setListeners()
	{
		table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Weapon>()
		{
			@Override
			public void changed(ObservableValue ov, Weapon arg1, Weapon arg2)
			{
				if(arg2 != null)
				{
					isSlotChange = true;
					weaponComboBox.getItems().remove(0); //remove previous slot's weapon from combobox
					currentSlotNum = arg2.getSlotNumber() - 1;
					weaponComboBox.getItems().add(0, "-" +
							editTools.getWeaponList().get(currentSlotNum).getWeaponName() + "-(UNCHANGED)");

					//the current slot's weapon's index
					int index =
							db.getWeaponIndex(
									editTools.getNewWeaponList().get(currentSlotNum).getWeaponName());
					if(arg1 == null || index == db.getWeaponList().size() || index == 0)
					{
						//if the previous slot didn't hold a weapon
						//or if the current weapon isn't in database (for weapons with unique bytes)
						weaponComboBox.getSelectionModel().selectFirst();
					}
					else
					{
						weaponComboBox.getSelectionModel().select(index + 1);
					}

					setWeaponAttribute(weaponSlot1ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[0].getName());
					setWeaponAttribute(weaponSlot2ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[1].getName());
					setWeaponAttribute(weaponSlot3ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[2].getName());
					setWeaponAttribute(weaponSlot4ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[3].getName());
					setWeaponAttribute(weaponSlot5ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[4].getName());
					setWeaponAttribute(weaponSlot6ComboBox, editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[5].getName());

					setWeaponRangedStars(rangedStarsComboBox, editTools.getNewWeaponList().get(currentSlotNum).getRangedStars());
					setWeaponMeleeStars(meleeStarsComboBox, editTools.getNewWeaponList().get(currentSlotNum).getMeleeStars());

					curWeaponValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getTotalValue());
					meleeStarsValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getMeleeStarsValue() + "");
					rangedStarsValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getRangedStarsValue() + "");
					weaponSlot1ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[0].getValue() + "");
					weaponSlot2ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[1].getValue() + "");
					weaponSlot3ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[2].getValue() + "");
					weaponSlot4ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[3].getValue() + "");
					weaponSlot5ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[4].getValue() + "");
					weaponSlot6ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[5].getValue() + "");

					updateAllValues();
					isSlotChange = false;
				}
			}
		});

		weaponComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			//this listener is called even during weapon slot changes
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(!isSlotChange && arg1 != null && arg2 != null && !arg1.isEmpty() && !arg2.isEmpty())
				{
					editTools.updateListWeapon(currentSlotNum, arg2); //old weapon index, new weapon name
				}
			}
		});

		meleeStarsComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListMeleeStars(currentSlotNum, arg2);
					meleeStarsValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getMeleeStarsValue() + "");
					updateValues(meleeStarsValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		rangedStarsComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListRangedStars(currentSlotNum, arg2);
					rangedStarsValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getRangedStarsValue() + "");
					updateValues(rangedStarsValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot1ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 0, arg2);
					weaponSlot1ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[0].getValue() + "");
					updateValues(weaponSlot1ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot2ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 1, arg2);
					weaponSlot2ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[1].getValue() + "");
					updateValues(weaponSlot2ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot3ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 2, arg2);
					weaponSlot3ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[2].getValue() + "");
					updateValues(weaponSlot3ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot4ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 3, arg2);
					weaponSlot4ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[3].getValue() + "");
					updateValues(weaponSlot4ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot5ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 4, arg2);
					weaponSlot5ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[4].getValue() + "");
					updateValues(weaponSlot5ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponSlot6ComboBox.valueProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue ov, String arg1, String arg2)
			{
				if(arg2 != null && !isSlotChange)
				{
					editTools.updateListAttribute(currentSlotNum, 5, arg2);
					weaponSlot6ValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getWeaponAttributes()[5].getValue() + "");
					updateValues(weaponSlot6ValueLabel);
				}
				//this listener is called even during weapon slot changes
			}
		});

		weaponComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	if(chars.equals("MINUS"))
            	{
            		weaponComboBox.getSelectionModel().select(0);
            	}
            	else
            	{
	            	int curSelection = weaponComboBox.getSelectionModel().getSelectedIndex();
	            	int newSelection = db.searchWeapon(chars, curSelection);
	            	if(newSelection > 0)
	            	{
	            		weaponComboBox.getSelectionModel().select(newSelection);
	            	}
            	}
            }
        });

		weaponSlot1ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot1ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot1ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });

		weaponSlot2ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot2ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot2ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });

		weaponSlot3ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot3ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot3ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });

		weaponSlot4ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot4ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot4ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });

		weaponSlot5ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot5ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot5ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });

		weaponSlot6ComboBox.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
            @Override
            public void handle(KeyEvent event)
            {
            	String chars = event.getCode() + "";
            	int curSelection = weaponSlot6ComboBox.getSelectionModel().getSelectedIndex();
            	int newSelection = db.searchAttribute(chars, curSelection);
            	if(newSelection >= 0)
            	{
            		weaponSlot6ComboBox.getSelectionModel().select(newSelection);
            	}
            }
        });
	}

	public void setWeaponAttribute(ComboBox cb, String attribute)
	{
		int index = db.getAttributeIndex(attribute);
		cb.getSelectionModel().select(index);
	}

	public void setWeaponRangedStars(ComboBox cb, Stars stars)
	{
		int index = db.getRangedStarsIndex(stars.getAmount());
		cb.getSelectionModel().select(index);
	}

	public void setWeaponMeleeStars(ComboBox cb, Stars stars)
	{
		int index = db.getMeleeStarsIndex(stars.getAmount());
		cb.getSelectionModel().select(index);
	}

	public void updateAllValues()
	{
		valueColorDecider(curWeaponValueLabel);
		valueColorDecider(meleeStarsValueLabel);
		valueColorDecider(rangedStarsValueLabel);
		valueColorDecider(weaponSlot1ValueLabel);
		valueColorDecider(weaponSlot2ValueLabel);
		valueColorDecider(weaponSlot3ValueLabel);
		valueColorDecider(weaponSlot4ValueLabel);
		valueColorDecider(weaponSlot5ValueLabel);
	    valueColorDecider(weaponSlot6ValueLabel);
		weaponTotalValueColorDecider();
	}

	public void updateValues(Label label)
	{
		curWeaponValueLabel.setText(editTools.getNewWeaponList().get(currentSlotNum).getTotalValue());
		weaponTotalValueColorDecider();
		valueColorDecider(label);
	}

	public void weaponTotalValueColorDecider()
	{
		boolean isOverValued = false;
		double value = Double.parseDouble(curWeaponValueLabel.getText());
		if(value >= 351)
		{
			curWeaponValueLabel.setStyle("-fx-text-fill:red");
			isOverValued = true;
		}
		else
		{
			curWeaponValueLabel.setStyle("-fx-text-fill:green");
		}
	}

	public void valueColorDecider(Label label)
	{
		double value = Double.parseDouble(label.getText());
		if(value == 0)
		{
			label.setStyle("-fx-text-fill:black");
		}
		else if(value > 0)
		{
			label.setStyle("-fx-text-fill:green");
		}
		else
		{
			label.setStyle("-fx-text-fill:red");
		}
	}

	public void clearAll()
	{
		//these call change listeners
		weaponComboBox.getItems().clear();
		meleeStarsComboBox.getItems().clear();
		rangedStarsComboBox.getItems().clear();
		weaponSlot1ComboBox.getItems().clear();
		weaponSlot2ComboBox.getItems().clear();
		weaponSlot3ComboBox.getItems().clear();
		weaponSlot4ComboBox.getItems().clear();
		weaponSlot5ComboBox.getItems().clear();
		weaponSlot6ComboBox.getItems().clear();
		table.getItems().clear();
	}
}