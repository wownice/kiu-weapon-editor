public class Stars
{
	private String starAmount;
	private String byteDec;
	private double value;

	public Stars(String amount, String dec, String theValue)
	{
		starAmount = amount;
		byteDec = dec;
		value = Double.parseDouble(theValue);
	}

	public String getAmount()
	{
		return starAmount;
	}

	public String getDec()
	{
		return byteDec;
	}

	public double getValue()
	{
		return value;
	}
}
