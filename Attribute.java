public class Attribute
{
	private String attributeName;
	private String attributeValueDec;
	private double value;

	public Attribute(String name, String decVal, String val)
	{
		attributeName = name;
		attributeValueDec = decVal;
		value = Double.parseDouble(val);
	}

	public String getName()
	{
		return attributeName;
	}

	public String getValueDec()
	{
		return attributeValueDec;
	}

	public double getValue()
	{
		return value;
	}
}
